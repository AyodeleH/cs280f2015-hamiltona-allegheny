# Seniors for 2014-2015
CMPSC 280 Fall 2015- Ayodele Hamilton
+ Ryan Cambier      - request submitted
+ Tristan Challener - no request submitted
+ Mackenzie Jordan  - no request submitted
+ Kara King         - request submitted
+ Michael Ligouri   - request submitted
+ Alden Page        - no request submitted
+ Marlee Sherretts  - request submitted
+ Willem Yarbrough  - no request submitted

# First and Second Reader Assignment

+ Cambier   - Kapfhammer, Jumadinova
+ Challener - Kapfhammer, Wenskovitch
+ Jordan    - Kapfhammer, Jumadinova
+ King      - Jumadinova, Kapfhammer
+ Ligouri   - Jumadinova, Wenskovitch
+ Page      - Jumadinova, Kapfhammer
+ Sherretts - Kapfhammer, Jumadinova
+ Yarbrough - Jumadinova, Kapfhammer

# First Reader Breakdown

+ Jumadinova  - King, Ligouri, Page, Yarbrough
+ Kapfhammer  - Cambier, Challener, Sherretts
+ Roos        - None, on sabbatical
+ Wenskovitch - None, visiting assistant professor

# Second Reader Breakdown

+ Jumadinova  - Cambier, Jordan, Sherretts
+ Kapfhammer  - King, Page, Yarbrough
+ Roos        - None, on sabbatical
+ Wenskovitch - Challener, Ligouri

